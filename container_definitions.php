<?php

use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;

require_once "vendor/autoload.php";

$paths = array("src/models");
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
$config->addEntityNamespace('', 'models');
// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'host'     => 'localhost',
    'user'     => 'kamiyab',
    'password' => '',
    'dbname'   => 'blog',
);

return [
    'Doctrine\ORM\EntityManager' => function (ContainerInterface $c) use ($dbParams, $config) {
        return \Doctrine\ORM\EntityManager::create($dbParams, $config);
    },
];
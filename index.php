<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

require __DIR__.'/config.php';

$routes = new RouteCollection();
$routes->add('blog', new Route('/'));
$routes->add('login', new Route('/login/'));
$routes->add('logout', new Route('/logout'));
$routes->add('register', new Route('/register'));
$routes->add('forgot', new Route('/password/forgot'));
$routes->add('reset', new Route('/password/reset'));
$routes->add('panel', new Route('/panel/new-post'));
$routes->add('profile', new Route('/profile'));
$routes->add('edit', new Route('/profile/edit'));

$context = new RequestContext();
$context->fromRequest(Request::createFromGlobals());

$matcher = new UrlMatcher($routes, $context);

try {
    $parameters = $matcher->match($context->getPathInfo());
}   catch (ResourceNotFoundException $exception) {
    echo "No such page found!";
}

if ($parameters['_route'] == 'blog') {
    require_once 'blog.php';
    die();
};

if ($parameters['_route'] == 'login') {
    require_once 'login/login.php';
    die();
}

if ($parameters['_route'] == 'logout') {
    require_once 'login/logout.php';
    die();
}

if ($parameters['_route'] == 'register') {
    require_once 'login/register.php';
    die();
}

if ($parameters['_route'] == 'forgot') {
    require_once 'login/forgot.php';
    die();
}

if ($parameters['_route'] == 'reset') {
    require_once 'login/reset.php';
    die();
}

if ($parameters['_route'] == 'panel') {
    require_once 'panel/panel.php';
    die();
}

if ($parameters['_route'] == 'profile') {
    require_once 'panel/profile.php';
    die();
}

if ($parameters['_route'] == 'edit') {
    require_once 'panel/edit.php';
    die();
}
<?php
use app\SessionManager;
use models\PasswordReset;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;


require __DIR__.'/../config.php';
require __DIR__.'/../form_setup.php';
require __DIR__.'/../bootstrap.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/../container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$session = $sessionManager->getSession();
$session->start();


if ($sessionManager->isLoggedIn()) {
    echo "<script>window.open('/blog/panel/new-post','_self')</script>";
    exit();
} else {
    $request = Request::createFromGlobals();
    $alert = [];

    $form = $formFactory->createBuilder()
        ->add('email', TextType::class)
        ->add('send', SubmitType::class)
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $formData = $form->getData();
        $email = $formData['email'];

        $author = $entityManager->getRepository(':Author')
            ->findOneBy(array('email' => $email));

        if ($author != null) {

            $selector = bin2hex(random_bytes(8));
            $validator = random_bytes(32);
            $expires = strtotime(date('Y-m-d H:i:s', strtotime('1 hour')));

            $passwordReset = new PasswordReset();
            $passwordReset->setAuthor($author);
            $passwordReset->setSelector($selector);
            $passwordReset->setValidator($validator);
            $passwordReset->setExpires($expires);

            $oldPasswordReset = $entityManager->getRepository(':PasswordReset')
                ->findOneBy(array('author' => $author));
            if ($oldPasswordReset != null) {
                $entityManager->remove($oldPasswordReset);
                $entityManager->flush();
            }

            $entityManager->persist($passwordReset);
            $entityManager->flush();

            $url = sprintf('%sreset.php?%s', 'localhost/blog/login/', http_build_query([
                'selector' => $selector,
                'validator' => bin2hex($validator)
            ]));

            $message = '<p>برای بازیابی رمز ورود روی لینک زیر کلیک کنید' . '<br>'
                .sprintf('<a href="%s">%s</a></p>', $url, $url);

            $mail->addAddress($email);
            $mail->Subject = 'Password reset link';
            $mail->Body = $message;
            try {
                $mail->send();
                $alert['mail_sent'] = 'sent';
            } catch (Exception $e) {
                $alert['mail_sent'] = 'sent';
            }
        }
    }

    echo $twig->render('forgot.twig', array(
        'alert' => $alert,
        'form' => $form->createView(),
        'session' => $sessionManager->getSession()));
}
?>
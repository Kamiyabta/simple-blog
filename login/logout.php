<?php
use app\SessionManager;

require __DIR__.'/../config.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/../container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$sessionManager->getSession()->start();

$sessionManager->logout();

echo "<script>window.open('/blog/','_self')</script>";

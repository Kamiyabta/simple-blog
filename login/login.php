<?php

use app\ValidationManager;
use models\Author;
use app\SessionManager;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__.'/../config.php';
require_once __DIR__.'/../form_setup.php';
require_once __DIR__.'/../bootstrap.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/../container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$sessionManager->getSession()->start();


if ($sessionManager->isLoggedIn()) {
    echo "<script>window.open('/blog/panel/new-post','_self')</script>";
} else {
    $request = Request::createFromGlobals();
    $errors = [];

    $form = $formFactory->createBuilder()
        ->add('email', TextType::class)
        ->add('password', PasswordType::class)
        ->add('login', SubmitType::class)
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $formData = $form->getData();
        $email = $formData['email'];
        $password = $formData['password'];

        $validationManager = $container->get(ValidationManager::class);
        $errors = $validationManager->loginValidation($email, $password);

        if (empty($errors)) {
            $sessionManager->login($email);
            echo "<script>window.open('/blog/panel/new-post','_self')</script>";
        }
    }

    echo $twig->render('login.twig', array(
        'errors' => $errors,
        'form' => $form->createView(),
        'session' => $sessionManager->getSession()));
}
?>
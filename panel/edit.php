<?php
use models\Author;
use app\SessionManager;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

require __DIR__.'/../config.php';
require __DIR__.'/../form_setup.php';
require __DIR__.'/../bootstrap.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/../container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$session = $sessionManager->getSession();
$session->start();

$email = $session->get('email');

if (!$sessionManager->isLoggedIn()) {
    echo "<script>window.open('/blog/login','_self')</script>";
} else {
    $request = Request::createFromGlobals();
    $errors = [];

    $author = $entityManager->getRepository(':Author')
        ->findOneBy(array('email' => $email));

    $form = $formFactory->createBuilder()
        ->add('name', TextType::class, array(
            'required' => false
        ))
        ->add('bio', TextType::class, array(
            'required' => false
        ))
        ->add('avatar', FileType::class, array(
            'required' => false
        ))
        ->add('about', FileType::class, array(
            'required' => false
        ))
        ->add('edit', SubmitType::class)
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $formData = $form->getData();
        $newName = $formData['name'];
        $newBio = $formData['bio'];

        if (strlen($newName) > 100)
            $errors['name_char'] = true;
        if (strlen($newBio) > 300)
            $errors['bioـchar'] = true;

        if (!$errors['name_char'] && !$errors['bio_char']) {
            $author->setName($newName);
            $author->setBio($newBio);
            $session->set('name', $newName);
        }

        $avatar = $formData['avatar'];
        if ($avatar !== null) {

            $avatarUuid = $author->getAvatar();
            $newAvatarUuid = uniqid(uniqid(), true);

            $avatar->move('../files/', $newAvatarUuid);

            $author->setAvatarUuid($newAvatarUuid);

            if ($avatarUuid != '') {
                unlink('../files/'.$avatarUuid);
            }
        }

        $about = $formData['about'];
        if ($about != null) {

            $aboutUuid = $author->getAbout();
            $newAboutUuid = uniqid(uniqid(uniqid(), true), false);

            $about->move('../files/', $newAboutUuid);

            $author->setAboutUuid($email, $newAboutUuid);

            if ($aboutUuid != '') {
                unlink('../files/'.$aboutUuid);
            }
        }
    }

    $entityManager->persist($author);
    $entityManager->flush();

    $name = $author->getName();
    $bio = $author->getBio();

    echo $twig->render('edit.twig', array(
        'errors' => $errors,
        'form' => $form->createView(),
        'session' => $session,
        'name' => $name,
        'bio' => $bio));
}
?>
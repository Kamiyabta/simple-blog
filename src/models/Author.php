<?php
namespace models;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="authors")
 **/
class Author
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string") **/
    protected $password;

    /** @Column(type="string") **/
    protected $email;

    /** @Column(type="string", nullable=true) **/
    protected $name;

    /** @Column(type="string", nullable=true) **/
    protected $bio;

    /** @Column(type="string", nullable=true) **/
    protected $avatar_uuid;

    /** @Column(type="string", nullable =true) **/
    protected $about_uuid;

    /**
     * @OneToMany(targetEntity="Post", mappedBy="author")
     */
    private $posts;

    function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }


    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getBio()
    {
        return $this->bio;
    }

    public function setBio($bio): void
    {
        $this->bio = $bio;
    }

    public function getAvatarUuid()
    {
        return $this->avatar_uuid;
    }

    public function setAvatarUuid($avatar_uuid): void
    {
        $this->avatar_uuid = $avatar_uuid;
    }

    public function getAboutUuid()
    {
        return $this->about_uuid;
    }

    public function setAboutUuid($about_uuid): void
    {
        $this->about_uuid = $about_uuid;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }



    /**
     * Add post.
     *
     * @param \models\Post $post
     *
     * @return Author
     */
    public function addPost(\models\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post.
     *
     * @param \models\Post $post
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePost(\models\Post $post)
    {
        return $this->posts->removeElement($post);
    }

    /**
     * Get posts.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }
}

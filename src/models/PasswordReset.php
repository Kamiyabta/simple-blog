<?php
namespace models;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @Entity @Table(name="password_reset")
 **/
class PasswordReset
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string") **/
    protected $selector;

    /** @Column(type="string")  **/
    protected $validator;

    /** @Column(type="bigint")  **/
    protected $expires;

    /**
     * @OneToOne(targetEntity="Author")
     * @JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;


    public function getId()
    {
        return $this->id;
    }


    public function getSelector()
    {
        return $this->selector;
    }


    public function setSelector($selector): void
    {
        $this->selector = $selector;
    }


    public function getValidator()
    {
        return $this->validator;
    }


    public function setValidator($validator): void
    {
        $this->validator = $validator;
    }


    public function getExpires()
    {
        return $this->expires;
    }


    public function setExpires($expires): void
    {
        $this->expires = $expires;
    }


    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set author.
     *
     * @param \models\Author|null $author
     *
     * @return PasswordReset
     */
    public function setAuthor(\models\Author $author = null)
    {
        $this->author = $author;

        return $this;
    }
}

<?php
namespace models;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="authors")
 **/
class Author
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string") **/
    protected $password;

    /** @Column(type="string") **/
    protected $email;

    /** @Column(type="string") **/
    protected $bio;

    /** @Column(type="string") **/
    protected $avatar_uuid;

    /** @Column(type="string") **/
    protected $about_uuid;

    /** @Column(type="datetime") **/
    protected $created_at;

    /** @Column(type="datetime") **/
    protected $updated_at;

    /**
     * @OneToMany(targetEntity="Post", mappedBy="author")
     */
    private $posts;

    function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }


    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getBio()
    {
        return $this->bio;
    }

    public function setBio($bio): void
    {
        $this->bio = $bio;
    }

    public function getAvatarUuid()
    {
        return $this->avatar_uuid;
    }

    public function setAvatarUuid($avatar_uuid): void
    {
        $this->avatar_uuid = $avatar_uuid;
    }

    public function getAboutUuid()
    {
        return $this->about_uuid;
    }

    public function setAboutUuid($about_uuid): void
    {
        $this->about_uuid = $about_uuid;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }



    /**
     * Add post.
     *
     * @param \models\Post $post
     *
     * @return Author
     */
    public function addPost(\models\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post.
     *
     * @param \models\Post $post
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePost(\models\Post $post)
    {
        return $this->posts->removeElement($post);
    }

    /**
     * Get posts.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }
}

<?php
namespace app;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionManager
{
    private $entityManager;
    private $session;

    function __construct(EntityManager $entityManager)
    {
        $this->session = new Session();
        $this->entityManager = $entityManager;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function isLoggedIn()
    {
        if ($this->session->has('email'))
            return true;
        else
            return false;
    }

    public function login($email)
    {
        $author = $this->entityManager->getRepository(':Author')
            ->findOneBy(array('email' => $email));

        $name = $author->getName();
        $this->session->set('email', $email);
        $this->session->set('name', $name);
    }

    public function logout()
    {
        $this->session->invalidate();
        return true;
    }

}